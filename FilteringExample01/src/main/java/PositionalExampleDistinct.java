import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/27/17.
 */
public class PositionalExampleDistinct {

    public static void main(String[] args){

        //generate observable with duplicated elements, scramble them.  print out
        Observable.from(DataGenerator.generateScrambledAndDuppedGreekAlphabet())
            .subscribe((letter) ->
                System.out.println(letter)
            );

        System.out.println("------------------------------------------------------------");
        System.out.println("------------------------------------------------------------");

        // Emit each string value only once, even if it appears in the
        // original list multiple times.
        Observable.from(DataGenerator.generateScrambledAndDuppedGreekAlphabet())
            .distinct()
            .subscribe((letter) ->
                System.out.println(letter)
            );

        System.out.println("------------------------------------------------------------");
        System.out.println("------------------------------------------------------------");

        System.exit(0);
    }
}
