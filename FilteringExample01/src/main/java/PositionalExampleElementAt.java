import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/27/17.
 */
public class PositionalExampleElementAt {

    public static void main(String[] args) {
        Observable.from(DataGenerator.generateGreekAlphabet())
            .elementAt(2)
            .subscribe((letter) ->
                    System.out.println(letter)
            );

        System.out.println();

        Observable.from(DataGenerator.generateGreekAlphabet())
            .elementAtOrDefault(50, "unknown")
            .subscribe((letter) ->
                    System.out.println(letter)
            );

        System.out.println();
        System.exit(0);
    }
}
