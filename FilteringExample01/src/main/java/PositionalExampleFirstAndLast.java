import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/27/17.
 */
public class PositionalExampleFirstAndLast {
    public static void main(String[] args) {

        Observable.from(DataGenerator.generateGreekAlphabet())
            .first(

            )
            .subscribe((letter) ->
                    System.out.println(letter)
            );
        System.out.println();

        Observable.from(DataGenerator.generateGreekAlphabet())
            .take(4)
            .subscribe((letter) ->
                    System.out.println(letter)
            );

        System.out.println();

        Observable.from(DataGenerator.generateGreekAlphabet())
            .last()
            .subscribe((letter) ->
                    System.out.println(letter)
            );

        System.out.println();

        Observable.from(DataGenerator.generateGreekAlphabet())
            .takeLast(4)
            .subscribe((letter) ->
                    System.out.println(letter)
            );

        System.out.println();

        Observable.empty()
            .firstOrDefault("List is empty.")
            .subscribe((letter) ->
                    System.out.println(letter)
            );

        System.out.println();

        Observable.empty()
            .lastOrDefault("List is empty")
            .subscribe((letter) ->
                    System.out.println(letter)
            );

        System.exit(0);
    }
}
