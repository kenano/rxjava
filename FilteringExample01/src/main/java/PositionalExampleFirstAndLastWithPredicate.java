import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/27/17.
 */
public class PositionalExampleFirstAndLastWithPredicate {
    public static void main(String[] args) {

        Observable.from(DataGenerator.generateGreekAlphabet())
            .first((letter) -> {
                return letter.equals("Beta");
            })
            .subscribe((letter) ->
                System.out.println(letter)
            );

        System.out.println();

        Observable.from(DataGenerator.generateGreekAlphabet())
            .last((letter) -> {
                return letter.equals("Gamma");
            })
            .subscribe((letter) ->
                System.out.println(letter)
            );

        System.exit(0);
    }
}
