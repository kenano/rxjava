import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/27/17.
 */
public class PredicatesExample {

    public static void main(String[] args) {
        Observable.from(DataGenerator.generateBigIntegerList())
            .filter((i) -> {
                return  ((i % 3 == 0) && (i < 20));
            })
            .subscribe((i) -> {
                System.out.println("i: " + i);
            });

        System.exit(0);
    }
}
