import utils.ThreadUtils;
import utils.TimeTicker;

import java.util.concurrent.TimeUnit;

/**
 * Created by kenanozdamar on 5/27/17.
 */
public class TimeBasedExampleSample {
    public static void main(String[] args){

        // TimeTicker is a class that generates an event every
        // 10 milliseonds.  The event is a Long that repres
        TimeTicker timeTicker = new TimeTicker(10);
        timeTicker.start();

        try {
            timeTicker.toObservable()
                .sample(1, TimeUnit.SECONDS)
                .subscribe((t) ->
                    System.out.println("tick: " + t)
                );

            // We do this for 10 seconds...
            ThreadUtils.sleep(10000);
        }finally {
            // ...and then stop the ticker...which will also call
            // onCompleted() on all observers.
            timeTicker.stop();
        }
        System.exit(0);
    }
}
