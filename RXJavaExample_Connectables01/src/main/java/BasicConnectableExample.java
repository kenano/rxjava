import rx.observables.ConnectableObservable;
import util.ThreadUtils;
import util.TimeTicker;

/**
 * Created by kenanozdamar on 6/12/17.
 */
public class BasicConnectableExample {
    public static void main(String[] args){

        //time ticker goes off every half second.
        TimeTicker timeTicker = new TimeTicker(500).start();

        //Convert the timeticker into an observable.
        //call publich() to get a ConnectableObservable
        ConnectableObservable<Long> connectable = timeTicker.toObservable().publish();

        //normally when subscribed is called events will be generated. This is not the case with ConnectableObservables.
        //events will not be generated until connect() is called.
        connectable.subscribe((t) -> {
            System.out.println("Tick" + ThreadUtils.currentThreadName() + " " + t);
        });

        //notice how for 3 seconds nothing happens until we call "connect"
        System.out.println("Sleeping for 3 seconds...");
        ThreadUtils.sleep(3000);

        // Now we call "connect" on the connectable observable...and things start happening...
        System.out.println("Connecting...");
        connectable.connect();

        // Let the ticker do its thing for 3 seconds so we can see events
        // flowing...
        ThreadUtils.sleep(3000);
        System.out.println("Three seconds are up!");

        // Stop the ticker and kill the example's VM
        timeTicker.stop();
        System.exit(0);
    }
}
