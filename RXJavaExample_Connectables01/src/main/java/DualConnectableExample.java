import rx.observables.ConnectableObservable;
import util.ThreadUtils;
import util.TimeTicker;

/**
 * Created by kenanozdamar on 6/12/17.
 *
 * Two subscribers that connect to a single ConnectableObservable
 *
 * Output:
 *Tick1TickerThread 1497319327545
 Tick2TickerThread 1497319327545
 Tick1TickerThread 1497319328048
 Tick2TickerThread 1497319328048
 Tick1TickerThread 1497319328553
 Tick2TickerThread 1497319328553
 Tick1TickerThread 1497319329058
 Tick2TickerThread 1497319329058
 Tick1TickerThread 1497319329560
 Tick2TickerThread 1497319329560
 Tick1TickerThread 1497319330065
 Tick2TickerThread 1497319330065

 Notice Threads are in order and in pairs. This is because both subscribers are running on the same thread. this is
 the default behavior.
 */
public class DualConnectableExample {

    public static void main(String[] args){

        //time ticker goes off every half second.
        TimeTicker timeTicker = new TimeTicker(500).start();

        //Convert the timeticker into an observable.
        //call publish() to get a ConnectableObservable
        ConnectableObservable<Long> connectable = timeTicker.toObservable().publish();

        //normally when subscribed is called events will be generated. This is not the case with ConnectableObservables.
        //events will not be generated until connect() is called.
        connectable.subscribe((t) -> {
            System.out.println("Tick1 " + ThreadUtils.currentThreadName() + " " + t);
        });

        //here another subscription is made the ConnectableObservable. This is the first time in the tutorial this is
        //being done. Any observable can be subscribed to multiple times.
        //ConnectableObservable gives you control over when the underlying observable begins to emit events.
        connectable.subscribe((t) -> {
            System.out.println("Tick2 " + ThreadUtils.currentThreadName() + " " + t);
        });

        //notice how for 3 seconds nothing happens until we call "connect"
        System.out.println("Sleeping for 3 seconds...");
        ThreadUtils.sleep(3000);

        // Now we call "connect" on the connectable observable...and things start happening...
        System.out.println("Connecting...");
        connectable.connect();

        // Let the ticker do its thing for 3 seconds so we can see events
        // flowing...
        ThreadUtils.sleep(3000);
        System.out.println("Three seconds are up!");

        // Stop the ticker and kill the example's VM
        timeTicker.stop();
        System.exit(0);

    }
}
