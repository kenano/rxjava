import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import util.ThreadUtils;
import util.TimeTicker;

/**
 * Created by kenanozdamar on 6/12/17.
 *
 * In this example one of the subscribers is set to execute on a separate thread.
 *
 * output:
 * Sleeping for 3 seconds...
 Connecting...
 Tick2 TickerThread 1497319679306
 Tick1 RxComputationThreadPool-1 1497319679306
 Tick1 RxComputationThreadPool-2 1497319679813
 Tick2 TickerThread 1497319679813
 Tick1 RxComputationThreadPool-1 1497319680320
 Tick2 TickerThread 1497319680320
 Tick1 RxComputationThreadPool-2 1497319680825
 Tick2 TickerThread 1497319680825
 Tick1 RxComputationThreadPool-3 1497319681331
 Tick2 TickerThread 1497319681331
 Tick1 RxComputationThreadPool-1 1497319681832
 Tick2 TickerThread 1497319681832
 Three seconds are up!

 Notice time events still in pairs but order is undefined.

 */
public class ObserveOnConnectableExample {
    public static void main(String[] args) {

        //time ticker goes off every half second.
        TimeTicker timeTicker = new TimeTicker(500).start();

        //Convert the timeticker into an observable.
        //call publish() to get a ConnectableObservable
        ConnectableObservable<Long> connectable = timeTicker.toObservable().publish();

        //normally when subscribed is called events will be generated. This is not the case with ConnectableObservables.
        //events will not be generated until connect() is called.
        connectable
                // We want to run this on a different thread than the ticker thread
                .observeOn(Schedulers.computation())
                .subscribe((t) -> {
            System.out.println("Tick1 " + ThreadUtils.currentThreadName() + " " + t);
        });

        //here another subscription is made the ConnectableObservable. This is the first time in the tutorial this is
        //being done. Any observable can be subscribed to multiple times.
        //ConnectableObservable gives you control over when the underlying observable begins to emit events.
        connectable
                // No observeOn specified...so this will be on the TickerThread
                .subscribe((t) -> {
            System.out.println("Tick2 " + ThreadUtils.currentThreadName() + " " + t);
        });


        //notice how for 3 seconds nothing happens until we call "connect"
        System.out.println("Sleeping for 3 seconds...");
        ThreadUtils.sleep(3000);

        // Now we call "connect" on the connectable observable...and things start happening...
        System.out.println("Connecting...");
        connectable.connect();

        // Let the ticker do its thing for 3 seconds so we can see events
        // flowing...
        ThreadUtils.sleep(3000);
        System.out.println("Three seconds are up!");

        // Stop the ticker and kill the example's VM
        timeTicker.stop();
        System.exit(0);

    }
}
