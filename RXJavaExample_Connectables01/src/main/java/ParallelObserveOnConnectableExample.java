import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import util.ThreadUtils;
import util.TimeTicker;

/**
 * Created by kenanozdamar on 6/12/17.
 *
 * In this example both subscribers are set to execute on a separate thread.
 *
 * output:
 * Sleeping for 3 seconds...
 Connecting...
 Tick2 RxComputationThreadPool-2 1497320007987
 Tick1 RxComputationThreadPool-3 1497320007987
 Tick1 RxComputationThreadPool-1 1497320008489
 Tick2 RxComputationThreadPool-4 1497320008489
 Tick1 RxComputationThreadPool-2 1497320008991
 Tick2 RxComputationThreadPool-3 1497320008991
 Tick1 RxComputationThreadPool-1 1497320009495
 Tick2 RxComputationThreadPool-5 1497320009495
 Tick1 RxComputationThreadPool-4 1497320010001
 Tick2 RxComputationThreadPool-6 1497320010001
 Tick1 RxComputationThreadPool-2 1497320010503
 Tick2 RxComputationThreadPool-7 1497320010503

 Ticks are still in pairs but now both threads are both on computation threads.


 */
public class ParallelObserveOnConnectableExample {

    public static void main(String[] args) {

        //time ticker goes off every half second.
        TimeTicker timeTicker = new TimeTicker(500).start();

        //Convert the timeticker into an observable.
        //call publish() to get a ConnectableObservable
        ConnectableObservable<Long> connectable = timeTicker.toObservable().publish();

        //normally when subscribed is called events will be generated. This is not the case with ConnectableObservables.
        //events will not be generated until connect() is called.
        connectable
                // We want to run this on a different thread than the ticker thread
                .observeOn(Schedulers.computation())
                .subscribe((t) -> {
                    System.out.println("Tick1 " + ThreadUtils.currentThreadName() + " " + t);
                });

        //here another subscription is made the ConnectableObservable. This is the first time in the tutorial this is
        //being done. Any observable can be subscribed to multiple times.
        //ConnectableObservable gives you control over when the underlying observable begins to emit events.
        connectable
                // We want to run this on a different thread than the ticker thread
                .observeOn(Schedulers.computation())
                .subscribe((t) -> {
                    System.out.println("Tick2 " + ThreadUtils.currentThreadName() + " " + t);
                });

        //notice how for 3 seconds nothing happens until we call "connect"
        System.out.println("Sleeping for 3 seconds...");
        ThreadUtils.sleep(3000);

        // Now we call "connect" on the connectable observable...and things start happening...
        System.out.println("Connecting...");
        connectable.connect();

        // Let the ticker do its thing for 3 seconds so we can see events
        // flowing...
        ThreadUtils.sleep(3000);
        System.out.println("Three seconds are up!");

        // Stop the ticker and kill the example's VM
        timeTicker.stop();
        System.exit(0);


    }
}
