import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import util.ThreadUtils;
import util.TimeTicker;

/**
 * Created by kenanozdamar on 6/12/17.
 *
 * Demonstrates of subscribers on different threads behave when one of the subscribers is running slow.
 *
 * output:
 * Sleeping for 3 seconds...
 Connecting...
 Tick2 RxComputationThreadPool-2 1497320461674
 Tick1 RxComputationThreadPool-3 1497320461674
 Tick1 RxComputationThreadPool-1 1497320462175
 Tick1 RxComputationThreadPool-4 1497320462677
 Tick2 RxComputationThreadPool-3 1497320462175
 Tick1 RxComputationThreadPool-1 1497320463180
 Tick1 RxComputationThreadPool-5 1497320463683
 Tick2 RxComputationThreadPool-4 1497320462677
 Tick1 RxComputationThreadPool-6 1497320464184
 Tick1 RxComputationThreadPool-2 1497320464690
 Tick2 RxComputationThreadPool-4 1497320463180
 Tick1 RxComputationThreadPool-1 1497320465191
 Tick2 RxComputationThreadPool-4 1497320463683
 Tick1 RxComputationThreadPool-8 1497320465698
 Tick1 RxComputationThreadPool-3 1497320466200
 Five seconds are up! STOPPING THE TICKER
 Tick2 RxComputationThreadPool-4 1497320464184
 Tick2 RxComputationThreadPool-4 1497320464690
 Tick2 RxComputationThreadPool-1 1497320465191
 Tick2 RxComputationThreadPool-1 1497320465698
 Tick2 RxComputationThreadPool-1 1497320466200
 *
 * All generated events from an observable are received, even after they are not being generated anymore.
 */
public class SlowParallelObserveOnConnectableExample {
    public static void main(String[] args) {

        //time ticker goes off every half second.
        TimeTicker timeTicker = new TimeTicker(500).start();

        //Convert the timeticker into an observable.
        //call publish() to get a ConnectableObservable
        ConnectableObservable<Long> connectable = timeTicker.toObservable().publish();

        //normally when subscribed is called events will be generated. This is not the case with ConnectableObservables.
        //events will not be generated until connect() is called.
        connectable
                // We want to run this on a different thread than the ticker thread
                .observeOn(Schedulers.computation())
                .subscribe((t) -> {
                    System.out.println("Tick1 " + ThreadUtils.currentThreadName() + " " + t);
                });

        //here another subscription is made the ConnectableObservable. This is the first time in the tutorial this is
        //being done. Any observable can be subscribed to multiple times.
        //ConnectableObservable gives you control over when the underlying observable begins to emit events.
        connectable
                // We want to run this on a different thread than the ticker thread
                .observeOn(Schedulers.computation())
                .subscribe((t) -> {
                    System.out.println("Tick2 " + ThreadUtils.currentThreadName() + " " + t);
                    ThreadUtils.sleep(1000);
                });

        // But notice how for 3 seconds nothing happens until we call "connect"
        System.out.println("Sleeping for 3 seconds...");
        ThreadUtils.sleep(3000);

        // Now we call "connect" on the connectable observable...and things start happening...
        System.out.println("Connecting...");
        connectable.connect();

        // Let the ticker do its thing for 3 seconds so we can see events
        // flowing...
        // One of our observers is slow...see what happens...
        ThreadUtils.sleep(5000);
        System.out.println("Five seconds are up! STOPPING THE TICKER");

        // Stop the ticker and kill the example's VM
        timeTicker.stop();

        // Wait another few seconds to let us catch up...
        ThreadUtils.sleep(5000);

        System.out.println( "Notice how the second observer continued to process its scheduled work...");
        System.exit(0);

    }
}
