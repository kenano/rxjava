package com.kenan;

import rx.Observable;

import java.util.Arrays;
import java.util.List;


public class Main {

    public static void main(String[] args) {

        List<String> shape_list  = Arrays.asList("rectangle", "square", "triangle");

        //we can create observables from certain structures (a list or perhaps any iterable) using from
        Observable<String> observable_string = Observable.from(shape_list);


        observable_string.subscribe(new SimpleObserver());

    }
}
