package com.kenan;

import rx.Observer;

/**
 * Created by kenanozdamar on 1/11/17.
 */
public class SimpleObserver implements Observer<String> {
    @Override
    public void onCompleted() {
        System.out.println("done...");
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("there was an error produced by the observable");
    }

    @Override
    public void onNext(String s) {
        System.out.println(s);
    }
}
