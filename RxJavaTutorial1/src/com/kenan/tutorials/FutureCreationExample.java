package com.kenan.tutorials;


import rx.Observable;
import rx.schedulers.Schedulers;

import java.util.List;
import java.util.concurrent.FutureTask;


/**
 * Created by kenanozdamar on 3/28/17.
 *
 * Wrap an Observable around a FutureTask interface.
 */
public class FutureCreationExample {

    public static void main(String[] args) {

        Observable<List<Integer>> observableFutureList;

        //Create a FutureTask that returns List<Integer>
        FutureTask<List<Integer>> future = new FutureTask<>(() -> {
            return DataGenerator.generateFibonacciList();
        });

        //Construct an observable. This creates an observable wrapper around
        //the future interface. future needs to be executed using its run
        //method
        observableFutureList = Observable.from(future);
//
//        //schedule the observable to run on the computation scheduler
        Schedulers.computation().schedule(() -> {
            future.run();
        });

        observableFutureList.subscribe((list) -> {
            list.forEach((i ) ->  {
                System.out.println(i);
             });
        });




        
    }

}
