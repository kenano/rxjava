package com.kenan.tutorials;

import com.kenan.tutorials.util.ThreadUtils;
import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;

import java.util.List;


/**
 * Created by kenanozdamar on 5/25/17.
 */
public class SubscriptionAllOneThread {

    public static void main(String[] args) {
        System.out.println("--------------------------------------");
        System.out.println("Creating an Observable that does not specify a subscribeOn or an observeOn Scheduler");
        System.out.println("driving thread: " + ThreadUtils.currentThreadName());
        System.out.println("--------------------------------------");

        //create a list of Integers
        List<Integer> integerList = DataGenerator.generateFibonacciList();

        //Wrap the above list in an Observable
        Observable<Integer> observable =  Observable.from(integerList);

        observable.subscribe(
                //onNext
                (i) -> {
                    System.out.println("onNext thread entr: " + ThreadUtils.currentThreadName());
                    System.out.println(i);
                    System.out.println("onNext thread exit: " + ThreadUtils.currentThreadName());
                },
                //error
                (throwable) -> {
                    throwable.printStackTrace();
                },
                ()->{
                    System.out.println("onCompleted ");
                }
        );

    }
}
