import utils.ThreadUtils;
import utils.TimeTicker;

import java.util.concurrent.TimeUnit;

/**
 * Created by kenanozdamar on 5/29/17.
 */
public class BufferExample {
    public static void main(String[] args) {

        // Create a ticker that will go off 10 times per second (100ms)
        TimeTicker timeTicker = new TimeTicker(100);
        timeTicker.start();

        timeTicker.toObservable()
                // We want to buffer and emit once every second...
                //so if each tick is emitted at a rate of 10 per second we should see about 10 events in the list
                .buffer(1, TimeUnit.SECONDS)
                .subscribe((list) -> {
                    System.out.println("----------------------------");
                    int count = 1;
                    int size = list.size();

                    for(int i = 0; i < size; i++){
                        System.out.println("" + (count++) + ": " + list.get(i));
                    }
                });

        // Do this for 5 seconds so we can see the effect.
        ThreadUtils.sleep(5000);

        // Stop the ticker.
        timeTicker.stop();

        System.exit(0);
    }
}
