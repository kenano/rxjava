import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/29/17.
 */
public class GeneralConditionalsExample {

    public static void main(String[] args){

        // defaultIfEmpty example - We create an empty observable
        // and then apply "defaultIfEmpty" and set the default to "Hello World".
        // Since the observable is empty, "Hello World" will be emitted as
        // the only event.
        Observable.empty()
                .defaultIfEmpty("Hello world")
                .subscribe((s) -> {
                    System.out.println(s);
                });

        System.out.println("--------------------------");

        // defaultIfEmpty example  2 - We create an non-empty observable
        // and then apply "defaultIfEmpty" and set the default to "Hello World".
        // Since the observable is not empty, the list items will be emitted.
        Observable.from(DataGenerator.generateGreekAlphabet())
                .first()
                .subscribe((s) -> {
                    System.out.println(s);
                });
        System.out.println("--------------------------");

        //skip if less than 8
        Observable.from(DataGenerator.generateFibonacciList())
                .skipWhile((i) -> {
                    return i < 8;
                })
                .subscribe((i) -> {
                      System.out.println(i);
                });

        System.out.println("--------------------------");

        //return only if less than 8 (basically inverse of above
        Observable.from(DataGenerator.generateFibonacciList())
                .takeWhile((i) -> {
                    return i < 8;
                })
                .subscribe((i) -> {
                     System.out.println(i);
                });

        System.out.println("--------------------------");

        //return only if the index from structure iterating through is lass than 3 (elements 0-2)
        Observable.from(DataGenerator.generateFibonacciList())
                .takeWhileWithIndex((i, index) -> {
                     return index < 3;
                })
                .subscribe((i) -> {
                    System.out.println(i);
                });
    }
}
