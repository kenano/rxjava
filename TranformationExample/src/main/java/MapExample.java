import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/29/17.
 */
public class MapExample {
    public static void main(String[] args) {
        Observable.from(DataGenerator.generateGreekAlphabet())
                .map((letterString) -> {
                    return letterString.toUpperCase();
                })
                .subscribe((letterString) -> {
                    System.out.println(letterString);
                });

        System.out.println("--------------------------------------------------");

        Observable.from(DataGenerator.generateGreekAlphabet())
                .flatMap((letterString) -> {
                    String[] returnString = {letterString.toUpperCase(), letterString.toUpperCase()};
                    return Observable.from(returnString);
                })
                .subscribe((letterString) -> {
                    System.out.println(letterString);
                });
    }
}
