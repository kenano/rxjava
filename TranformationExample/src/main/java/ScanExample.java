import rx.Observable;
import utils.DataGenerator;

/**
 * Created by kenanozdamar on 5/29/17.
 */
public class ScanExample {
    public static void main(String[] args){
        Observable.from(DataGenerator.generateGreekAlphabet())
                .scan(new StringBuilder(), (accumulatorBuffer, nextLetter) -> {
                    return accumulatorBuffer.append(nextLetter);
                })
                .subscribe((total) -> {
                    System.out.println("Scan Event: " + total.toString());
                });
        System.out.println("--------------------------------------------------");

        Observable.from(DataGenerator.generateGreekAlphabet())
                .scan(new StringBuilder(), (accumulatorBuffer, nextLetter) -> {
                    return accumulatorBuffer.append(nextLetter);
                })
                .last()
                .subscribe((total) -> {
                    System.out.println("Scan Event: " + total.toString());
                });
        System.out.println("--------------------------------------------------");

        System.exit(0);
    }
}
