import rx.Observable;
import utils.DataGenerator;
import utils.ThreadUtils;
import utils.TimedEventSequence;

/**
 * Created by kenanozdamar on 5/29/17.
 *
 * RxJava has time and race conditional based operations.
 * This example explores them.
 */
public class TimedConditionalExampleAmbiguous {
    public static void main(String[] args){

        // "Ambiguous" example - First, we create two TimedEventSequence
        // instances...one that emits Greek letters with a shorter interval,
        // the other that emits English letters on a longer interval.
        TimedEventSequence<String> sequence1 = new TimedEventSequence<>(DataGenerator.generateGreekAlphabet(), 50);
        TimedEventSequence<String> sequence2 = new TimedEventSequence<>(DataGenerator.generateEnglishAlphabet(), 100);

        //print out whatever observable first produces an event.
        //since sequence1 has shorter break intervals producing output, it should be first.
        Observable.amb(sequence1.toObservable(), sequence2.toObservable())
                .subscribe((s) -> {
                    System.out.println(s);
                });

        // Start the driver thread for each of these sequences.
        sequence1.start();
        sequence2.start();

        // Wait for 4 seconds while things run...we should see Greek letters
        // being emitted at 50ms intervals for 4 seconds since the first
        // sequence to emit events will be selected by the amb operator.
        ThreadUtils.sleep(4000);

        // Stop each sequence.
        sequence1.stop();
        sequence2.stop();

        System.exit(0);

    }
}
